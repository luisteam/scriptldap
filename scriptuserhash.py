import ldap
from ldap import modlist
import getpass
import csv
import subprocess

#python3 -m pip install python-ldap
#ldapsearch -x -LLL -D 'cn=admin,dc=rajoy,dc=gonzalonazareno,dc=org' -W -b 'ou=Group,dc=rajoy,dc=gonzalonazareno,dc=org'

conection = "ldap://172.22.200.141:389/"
dom = 'dc=rajoy,dc=gonzalonazareno,dc=org'
uidNumber = 2033
gidNumber = 2000


user = 'admin'
passwd = getpass.getpass('Clave del usuario %s LDAP: ' % user)

####################
with open('usuarios.csv') as csvfile:
    usuarios = csv.reader(csvfile.readlines(), delimiter=':')
###################

# Poblar LDAP
try:
        bind = "cn=%s,%s" % (user, dom)
        l = ldap.initialize(conection)
        l.simple_bind_s(bind,passwd)

        for i in usuarios:
                nombre = i[0]
                apellidos = i[1]
                uid = str(i[3])
                passwd2 = subprocess.check_output("sudo slappasswd -h {SSHA} -s root", shell=True)
                attrs = {}
                dn="uid=%s,ou=Group,dc=rajoy,dc=gonzalonazareno,dc=org" % str(i[3])
                attrs['objectClass'] = ['top', 'posixAccount', 'inetOrgPerson']
                attrs['uid'] = uid
                attrs['cn'] = nombre
                attrs['sn'] = apellidos
                attrs['mail'] = str(i[2])
                attrs['uidNumber'] = str(uidNumber)
                attrs['gidNumber'] = str(gidNumber)
                attrs['homeDirectory'] = '/home/%s' % uid
                attrs['loginShell'] = '/bin/bash'
                attrs['userPassword'] = passwd2
                ldif = modlist.addModlist(attrs)
                try:
                        l.add_s(dn,ldif)
                        uidNumber = uidNumber + 1
                        print('Usuario %s insertado.' % uid)
                except:
                        print("El usuario %s ya existe o no agregado." % str(i[3]))
        l.unbind_s()
except ldap.LDAPError, e:
 print('ERROR: ' + e[0]['desc'])

