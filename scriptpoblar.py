import ldap
from ldap import modlist
import getpass
import csv

#python3 -m pip install python-ldap

conection = "ldap://172.22.200.141:389/"
dom = 'dc=rajoy,dc=gonzalonazareno,dc=org'
uidNumber = 2000
gidNumber = 2002


user = 'admin'
passwd = getpass.getpass('Clave del usuario %s LDAP: ' % user)

####################
with open('alumnos.csv') as csvfile:
    alumnos = csv.reader(csvfile.readlines(), delimiter=':')
###################
with open('equipos.csv') as csvfile1:
    equipos = csv.reader(csvfile1.readlines(), delimiter=':')
####################

# Poblar LDAP
try:
	bind = "cn=%s,%s" % (user, dom)
	l = ldap.initialize(conection)
	l.simple_bind_s(bind,passwd)

	for i in alumnos:
		nombre = i[0]
		apellidos = i[1]
		uid = str(i[3])
		attrs = {}
		dn="uid=%s,ou=People,dc=rajoy,dc=gonzalonazareno,dc=org" % str(i[3])
		attrs['objectClass'] = ['top', 'posixAccount', 'inetOrgPerson', 'ldapPublicKey']
		attrs['uid'] = uid
		attrs['cn'] = nombre
		attrs['sn'] = apellidos
		attrs['mail'] = str(i[2])
		attrs['uidNumber'] = str(uidNumber)
		attrs['gidNumber'] = str(gidNumber)
		attrs['homeDirectory'] = '/home/%s' % uid
		attrs['loginShell'] = '/bin/bash'
		attrs['sshPublicKey'] = str(i[4])
		ldif = modlist.addModlist(attrs)
		try:
			l.add_s(dn,ldif)
			uidNumber = uidNumber + 1
			print('Usuario %s insertado.' % uid)
		except:
			print("El usuario %s ya existe o no agregado." % str(i[3]))
	for i in equipos:
		cn = str(i[0])
		ipHostNumber = str(i[1])
		sshPublicKey = str(i[2])
		attrs1 = {}
		dn1="uid=%s,ou=Computers,dc=rajoy,dc=gonzalonazareno,dc=org" % ipHostNumber
		attrs1['objectclass'] = ['top','device','ldapPublicKey','ipHost']
		attrs1['cn'] = cn
		attrs1['ipHostNumber'] = ipHostNumber
		attrs1['sshPublicKey'] = sshPublicKey
		ldif = modlist.addModlist(attrs1)
		try:
			l.add_s(dn1,ldif)
			print('Equipo %s insertado.' % ipHostNumber)
		except:
			print('El equipo %s ya existe o no agregado.' % ipHostNumber)
	l.unbind_s()
except ldap.LDAPError, e:
 print('ERROR: ' + e[0]['desc']) 
